import 'package:flutter/material.dart';

class MultTable extends StatelessWidget {
  final int size;

  MultTable(this.size);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(125, 174, 56, 1.0),
      child: GridView(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: size + 1, mainAxisSpacing: 1, crossAxisSpacing: 1),
        children: _getChildren(size),
        shrinkWrap: true,
      ),
    );
  }
  List<Widget> _getChildren(int size, [int index = 0]) {
    List<Widget> widgets = [];

    for (int i = 0; i <= size; i++) {
      widgets.add(
        Container(
          color: _chooseColor(i, index),
          child: Center(
            child: Text((i * index == 0 ? i == 0 ? index : i : i * index)
                .toString()),
          ),
        ),
      );
    }

    if (index != size) widgets.addAll(_getChildren(size, index + 1));

    return widgets;
  }

  Color _chooseColor(int localIndex, int globalIndex ){
    if(localIndex == 0 || globalIndex == 0)
      return Color.fromRGBO(163, 200, 69, 1.0);
    if(localIndex == globalIndex)
      return Color.fromRGBO(255, 245, 133, 1.0);
    if(localIndex < globalIndex)
      return Color.fromRGBO(254, 246, 185, 1.0);
    else
      return Colors.white;
  }
}
