import 'package:flutter/material.dart';
import 'package:multiplication_table/mult_table.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light(),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Multiplication Table"),
        ),
        body:MultTable(11),
      ),
    );
  }
}
